import { TextLineStream } from '@std/streams';

import type { InputMessage, OutputMessage } from './types.ts';

export function readLines(readable: ReadableStream<Uint8Array>): ReadableStream<string> {
  return readable
    .pipeThrough(new TextDecoderStream())
    .pipeThrough(new TextLineStream());
}

/**
 * Parse strfy messages from stdin.
 * strfry may batch multiple messages at once.
 *
 * @example
 * ```ts
 * // Loop through strfry input messages
 * for await (const msg of readStdin()) {
 *   // handle `msg`
 * }
 * ```
 */
export async function* readStdin(): AsyncIterable<InputMessage> {
  for await (const line of readLines(Deno.stdin.readable)) {
    try {
      yield JSON.parse(line);
    } catch (e) {
      console.error(line);
      throw e;
    }
  }
}

/** Writes the output message to stdout. */
export function writeStdout(msg: OutputMessage): void {
  console.log(JSON.stringify(msg));
}
