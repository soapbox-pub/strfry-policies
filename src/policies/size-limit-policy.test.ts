import { assertEquals } from '@std/assert';
import { buildEvent, buildInputMessage } from '@/test.ts';

import sizeLimitPolicy from './size-limit-policy.ts';

Deno.test('Size limit policy test', async () => {
  // Create a message under 12KB
  const smallContent = 'Hello'.repeat(100); // Well under 12KB
  const smallMessage = buildInputMessage({ event: buildEvent({ content: smallContent }) });

  // Create a message over 12KB
  const largeContent = 'Hello'.repeat(2500); // Over 12KB
  const largeMessage = buildInputMessage({ event: buildEvent({ content: largeContent }) });

  // Test that a small message is accepted
  assertEquals((await sizeLimitPolicy(smallMessage)).action, 'accept', 'Small message should be accepted');

  // Test that a large message is rejected
  assertEquals((await sizeLimitPolicy(largeMessage)).action, 'reject', 'Large message should be rejected');
});
