import { Policy } from '@/types.ts';

/** Policy options for `sizeLimitPolicy`. */
interface SizeLimitOptions {
  /** Maximum size of the message content in bytes. Default: 8KB (8192 bytes) */
  maxContentSize?: number;
  /** List of excluded event kinds */
  excludeKinds?: number[];
}

/**
 * Reject events larger than a specified size. Only applies to regular events (kind is less than 10000), as all other kinds are replaceable or ephemeral anyway.
 *
 * @example
 * ```ts
 * // Reject events larger than a custom size (15KB) and exclude event kinds [1, 2].
 * sizeLimitPolicy(msg, { maxContentSize: 15360, excludeKinds: [1, 2] });
 * // Reject events larger than the default size (8KB) and exclude event kinds [3].
 * sizeLimitPolicy(msg, { excludeKinds: [3] });
 * ```
 */
const sizeLimitPolicy: Policy<SizeLimitOptions> = ({ event: { id, content, kind } }, opts = {}) => {
  const {
    excludeKinds = [3], // Follows aka Contact Lists (NIP-02)
    maxContentSize = 8 * 1024, // Default size limit
  } = opts;

  // Convert the content into bytes and check its size
  const contentSize = new TextEncoder().encode(content).length;

  if (contentSize > maxContentSize && !excludeKinds.includes(kind) && kind < 10000) {
    return {
      id,
      action: 'reject',
      msg: `blocked: message is too large.`,
    };
  }

  return {
    id,
    action: 'accept',
    msg: '',
  };
};

export default sizeLimitPolicy;
